require 'json'
require_relative '../menu_bar'
require_relative './organizer'

class DiggNews < Organizer
    BASE_URL = "http://digg.com/api/news/popular.json" #constante para trabajar el url
    digg = []
    def self.digg_bank 
        digg = RestClient.get BASE_URL  #RestClient me realiza la descarga de la data en el url
        digg = JSON.parse digg
        digg ["data"]["feed"].each do |a|
        @@show_db << Organizer.new(a["content"]["title_alt"],a["content"]["author"],Time.at(a["date"]).strftime('%d/%m/%Y'),a["content"]["original_url"]) 
        #Nota para jefferson del futuro aca coloque ["content"] en todos salvo en la fecha ["date"] ya que la ultima estaba una carpeta antes que las demas y era necesario colocarlo para recorrerlo.
        end
        Organizer.show_data(@@show_db)
    end
end
