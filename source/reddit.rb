require 'json'
require 'rest-client' #el cual me permite descargar la data de reddit
require_relative './organizer'
require_relative '../menu_bar'

class RedditNews < Organizer
BASE_URL = "https://www.reddit.com/.json" #constante para trabajar el url
reddit = []
def self.reddit_bank 
    reddit = RestClient.get BASE_URL  #RestClient me realiza la descarga de la data en el url
    reddit = JSON.parse reddit
    reddit["data"]["children"].each do |e|
    @@show_db << Organizer.new(e["data"]["title"],e["data"]["author"],Time.at(e["data"]["created"]).strftime('%d/%m/%Y'),e["data"]["url"]) 
        end
        Organizer.show_data(@@show_db)
    end
end
